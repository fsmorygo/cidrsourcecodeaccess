// Copyright 2015 Philipp Smorygo

#include "AppCodeSourceCodeAccessPrivatePCH.h"
#include "AppCodeSourceCodeAccessor.h"
#include "DesktopPlatformModule.h"

DEFINE_LOG_CATEGORY_STATIC(LogAppCodeAccessor, Log, All);

#define LOCTEXT_NAMESPACE "AppCodeSourceCodeAccessor"

void FAppCodeSourceCodeAccessor::Startup()
{
	HasAppCode = DiscoverAppCode();
}

void FAppCodeSourceCodeAccessor::Shutdown() {}

bool FAppCodeSourceCodeAccessor::CanAccessSourceCode() const
{
	return HasAppCode;
}

FName FAppCodeSourceCodeAccessor::GetFName() const
{
	return FName("AppCodeSourceCodeAccessor");
}

FText FAppCodeSourceCodeAccessor::GetNameText() const
{
	return LOCTEXT("AppCodeDisplayName", "AppCode");
}

FText FAppCodeSourceCodeAccessor::GetDescriptionText() const
{
	return LOCTEXT("AppCodeDisplayDesc", "Open source code files in AppCode");
}

bool FAppCodeSourceCodeAccessor::OpenSolution()
{
	if (IsIDERunning())
	{
		STUBBED("OpenSolution: AppCode is running");
		return false;
	}
	
	FString Solution = GetSolutionPath();
	FString IDEPath;
	if (!HasAppCode)
	{
		UE_LOG(LogAppCodeAccessor, Warning, TEXT("FAppCodeSourceCodeAccessor::OpenSourceFiles: cannot find AppCode binary"));
		return false;
	}

	FProcHandle Proc = FPlatformProcess::CreateProc(*IDEPath, *Solution, true, false, false, nullptr, 0, nullptr, nullptr);
	if (Proc.IsValid())
	{
		FPlatformProcess::CloseProc(Proc);
		return true;
	}
	return false;
}

bool FAppCodeSourceCodeAccessor::OpenFileAtLine(const FString& FullPath, int32 LineNumber, int32 ColumnNumber)
{
	// column & line numbers are 1-based, so dont allow zero
	if(LineNumber == 0)
	{
		LineNumber++;
	}
	if(ColumnNumber == 0)
	{
		ColumnNumber++;
	}

	// Automatically fail if there's already an attempt in progress
	if (IsIDERunning())
	{
		// use qdbus
		STUBBED("OpenFileAtLine: AppCode is running");
		return false;
	}

	STUBBED("FAppCodeSourceCodeAccessor::OpenFileAtLine");
	return false;
}

bool FAppCodeSourceCodeAccessor::OpenSourceFiles(const TArray<FString>& AbsoluteSourcePaths)
{
	if (IsIDERunning())
	{
		// use qdbus
		STUBBED("OpenSourceFiles: AppCode is running");
		return false;
	}

	STUBBED("FAppCodeSourceCodeAccessor::OpenSourceFiles");
	return false;
}

bool FAppCodeSourceCodeAccessor::AddSourceFiles(const TArray<FString>& AbsoluteSourcePaths, const TArray<FString>& AvailableModules)
{
	return false;
}

bool FAppCodeSourceCodeAccessor::SaveAllOpenDocuments() const
{
	STUBBED("FAppCodeSourceCodeAccessor::SaveAllOpenDocuments");
	return false;
}

void FAppCodeSourceCodeAccessor::Tick(const float DeltaTime)
{
}


bool FAppCodeSourceCodeAccessor::DiscoverAppCode() const {
	if (IFileManager::Get().DirectoryExists(TEXT("/Applications/AppCode.app"))) {
		AppCodeFileName = TEXT("/Applications/AppCode.app");
		return true;
	}
	if (IFileManager::Get().DirectoryExists(TEXT("/Applications/AppCode EAP.app"))) {
		AppCodeFileName = TEXT("/Applications/AppCode EAP.app");
		return true;
	}
	return false;
}

bool FAppCodeSourceCodeAccessor::IsIDERunning()
{
	// FIXME: implement
	STUBBED("FAppCodeSourceCodeAccessor::IsIDERunning");
	return false;
}

FString FAppCodeSourceCodeAccessor::GetSolutionPath() const
{
	if(IsInGameThread())
	{
		FString SolutionPath;
		if(FDesktopPlatformModule::Get()->GetSolutionPath(SolutionPath))
		{
			CachedSolutionPath = FPaths::ConvertRelativePathToFull(SolutionPath);
		}
	}
	return CachedSolutionPath;
}

#undef LOCTEXT_NAMESPACE