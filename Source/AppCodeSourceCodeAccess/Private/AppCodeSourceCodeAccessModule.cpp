// Copyright 2015 Philipp Smorygo

#include "AppCodeSourceCodeAccessPrivatePCH.h"
#include "Runtime/Core/Public/Features/IModularFeatures.h"
#include "AppCodeSourceCodeAccessModule.h"

IMPLEMENT_MODULE( FAppCodeSourceCodeAccessModule, AppCodeSourceCodeAccess );

void FAppCodeSourceCodeAccessModule::StartupModule()
{
	AppCodeSourceCodeAccessor.Startup();

	// Bind our source control provider to the editor
	IModularFeatures::Get().RegisterModularFeature(TEXT("SourceCodeAccessor"), &AppCodeSourceCodeAccessor );
}

void FAppCodeSourceCodeAccessModule::ShutdownModule()
{
	// unbind provider from editor
	IModularFeatures::Get().UnregisterModularFeature(TEXT("SourceCodeAccessor"), &AppCodeSourceCodeAccessor);

	AppCodeSourceCodeAccessor.Shutdown();
}