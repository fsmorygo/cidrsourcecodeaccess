// Copyright 2015 Philipp Smorygo

#pragma once

#include "AppCodeSourceCodeAccessor.h"

class FAppCodeSourceCodeAccessModule : public IModuleInterface
{
public:
	/** IModuleInterface implementation */
	virtual void StartupModule() override;
	virtual void ShutdownModule() override;

private:
	FAppCodeSourceCodeAccessor AppCodeSourceCodeAccessor;
};