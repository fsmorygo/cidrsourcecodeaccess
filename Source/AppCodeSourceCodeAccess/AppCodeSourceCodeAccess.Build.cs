// Copyright 2015 Philipp Smorygo

namespace UnrealBuildTool.Rules
{
	public class AppCodeSourceCodeAccess : ModuleRules
	{
        public AppCodeSourceCodeAccess(TargetInfo Target)
		{
			PrivateDependencyModuleNames.AddRange(
				new string[]
				{
					"Core",
					"SourceCodeAccess",
					"DesktopPlatform"
				}
			);
		}
	}
}